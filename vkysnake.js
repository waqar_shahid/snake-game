window.onload = function()
{
    var canvas = document.createElement('canvas'),
    ctx = canvas.getContext('2d'),
    score = 0,
    direction = 0,
    snake = new Array(5),
    active = true,
    speed = 100;

    // Initialize the matrix.
    var map = new Array(800);
    for (var i = 0; i < map.length; i++)
    {
        map[i] = new Array(800);
    }

    canvas.width = 802;
    canvas.height = 334;

    var body = document.getElementsByTagName('body')[0];
    body.appendChild(canvas);

    // Add the snake
    map = generateSnake(map);

    // Add the food
    map = generateFood(map);

    drawGame();

    window.addEventListener('keydown', function(e) 
    {
        if (e.keyCode === 38 && direction !== 3)
        {
            direction = 2; // Up
        } else if (e.keyCode === 40 && direction !== 2) 
        {
            direction = 3; // Down
        } else if (e.keyCode === 37 && direction !== 0) 
        {
            direction = 1; // Left
        } else if (e.keyCode === 39 && direction !== 1) 
        {
            direction = 0; // Right
        }
    });

    function drawGame() 
    {
        // Clear the canvas
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        // Traverse all the body pieces of the snake, starting from the last one
        for (var i = snake.length - 1; i >= 0; i--) 
        {
            if (i === 0) 
            {
                switch(direction) 
                {
                    case 0: // Right
                        snake[0] = { x: snake[0].x + 1, y: snake[0].y }
                        break;
                    case 1: // Left
                        snake[0] = { x: snake[0].x - 1, y: snake[0].y }
                        break;
                    case 2: // Up
                        snake[0] = { x: snake[0].x, y: snake[0].y - 1 }
                        break;
                    case 3: // Down
                        snake[0] = { x: snake[0].x, y: snake[0].y + 1 }
                        break;
                }
                if (snake[0].x < 50 || 
                    snake[0].x >= 78 ||
                    snake[0].y < 0 ||
                    snake[0].y >= 30) 
                {
                    showGameOver();
                    return;
                }


                if (map[snake[0].x][snake[0].y] === 1) 
                {
                    score += 10;
                    map = generateFood(map);

                    // Add a new body piece to the array 
                    snake.push({ x: snake[snake.length - 1].x, y: snake[snake.length - 1].y });
                    map[snake[snake.length - 1].x][snake[snake.length - 1].y] = 2;
                

                } else if (map[snake[0].x][snake[0].y] === 2) 
                {
                    showGameOver();
                    return;
                }

                map[snake[0].x][snake[0].y] = 2;
            } else {

                if (i === (snake.length - 1)) {
                    map[snake[i].x][snake[i].y] = null;
                }

                snake[i] = { x: snake[i - 1].x, y: snake[i - 1].y };
                map[snake[i].x][snake[i].y] = 2;
            }
        }

        // Draw the border as well as the score
        drawMain();

        // Start cycling the matrix
        for (var x = 0; x < map.length; x++) {
            for (var y = 0; y < map[0].length; y++) {
                if (map[x][y] === 1) {
                    ctx.fillStyle = 'black';
                    ctx.fillRect(x * 10, y * 10 + 20, 10, 10);
                } else if (map[x][y] === 2) {
                    ctx.fillStyle = 'black';
                    ctx.fillRect(x * 10, y * 10 + 20, 10, 10);          
                }
            }
        }
        
        if (active) {
            setTimeout(drawGame, speed);
        }
    }

    function drawMain() 
    {
        ctx.lineWidth = 2; // Our border will have a thickness of 2 pixels
        ctx.strokeStyle = 'black'; // The border will also be black

        ctx.strokeRect(500, 20, 280, 300);

        ctx.fillStyle = 'black';
        ctx.font = '16px Calibri';
        ctx.fillText('Score: ' + score, 500, 334);
    }

    function generateFood(map)
    {
        // Generate a random position for the rows and the columns.
        var rndX = Math.round(Math.random() * 77),
            rndY = Math.round(Math.random() * 29);

        while (map[rndX][rndY] === 2) {
            rndX = Math.round(Math.random() * 77);
            rndY = Math.round(Math.random() * 29);
        }
         if (rndX <= 50)
        {
            if (rndX <= 10)
            {
            rndX = rndX + 66;
            }
            else if (rndX <= 20 && rndX>10)
            {
            rndX = rndX + 41;
            }
            else if (rndX <= 30 && rndX>20)
            {
            rndX = rndX + 31;
            }
            else if (rndX <= 40 && rndX>30)
            {
            rndX = rndX + 21;
            }
            else if (rndX <= 50 && rndX>40)
            {
            rndX = rndX + 11;
            }
        }         
        map[rndX][rndY] = 1;

        return map;
    }

    function generateSnake(map)
    {
        var rndX = 55;
        var rndY = 5;
        alert("Hello User :-)");
        while ((rndX - snake.length) < 0) 
        {
            rndX = Math.round(Math.random() * 19);
        }
        
        for (var i = 0; i < snake.length; i++) 
        {
            snake[i] = { x: rndX - i, y: rndY };
            map[rndX - i][rndY] = 2;
        }

        return map;
    }

    function showGameOver()
    {
        active = false;

        ctx.clearRect(0, 0, canvas.width, canvas.height);

        ctx.fillStyle = 'black';
        ctx.font = 'bold 20px arial';
        
        ctx.fillText('Game Over!', ((canvas.width / 2) - (ctx.measureText('Game Over!').width / 2))+240, 50);

        ctx.font = 'italic bold 12pt Courier';
        ctx.fillStyle = 'blue';
        ctx.fillText('Your Score Was: ' + score, ((canvas.width / 2) - (ctx.measureText('Your Score Was: ' + score).width / 2))+240, 70);   
    }
};